"use strict"

function TextInShape(scene, x, y, text, style) {
	const text = this.add.Text(x, y, text, style);
	
    const type = style["shape"]["type"];
    const parameters = style["shape"]["parameters"];
    const fill = style["shape"]["fill"];
    const stroke = style["shape"]["stroke"];
    const graphics = this.add.graphics();
    
    const grapic.generateTexture(text.canvas);
    
    graphics.fillStyle(fill["color"], fill["alpha"]);
    graphics.fillStyle(fill["color"], fill["alpha"]);
    
    switch() {
        case "rect":
            let rect = fillRectShape(parameters["x"], parameters["y"])
	        graphics.fillRectShape(rect);
	    break;
	}
	
	return graphics;
	
}
